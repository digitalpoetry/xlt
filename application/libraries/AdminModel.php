<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Model
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


/**
 * Admin Model.
 */
class AdminModel extends Myth\Models\CIDbModel {

    /**
     * @var Boolean $soft_deletes Set true to enable soft deletes.
     */
    protected $soft_deletes = true;

    /**
     * If set to true, logs the user id for 'created_by', 'modified_by' and 'deleted_by'.
     * 
     * @var Boolean $log_user Set true to enable user logging.
     */
    protected $log_user = true;

}
