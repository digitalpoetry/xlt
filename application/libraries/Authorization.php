<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Library
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


/**
 * Authorization Library, {@inheritDoc}.
 */
class Authorization extends \Myth\Auth\FlatAuthorization {

    /**
     * Given a group, will return the IDs of users in the group. `$group` can be
     * either the ID or the name of the group.
     *
     * @param  int|str $group     The group ID or name.
     * @return array    $user_ids An array of user IDs for the given group.
     */
    public function getUserIDsByGroup( $group )
    {
        /* Get the group id if a group name was given. */
        if (!is_numeric($group))
        {
            $group = $this->getGroupID($group);
        }

        /* Get user IDs by group ID. */
        $user_ids = $this->groupModel->select('auth_groups_users.user_id')
                                     ->join('auth_groups_users', 'auth_groups_users.group_id = auth_groups.id', 'left')
                                     ->where('auth_groups.id', (int) $group)
                                     ->find_all();

        /* Format array to be unassociative. */
        array_walk($user_ids, function(&$user) {
            $user = $user->user_id;
        });

        /* Return the array of user ID's for the group. */
        return $user_ids;
    }    

}
