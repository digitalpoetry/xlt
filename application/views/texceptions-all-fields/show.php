<h2>View Time Exception</h2>

<table class="table table-hover">
    <tbody>
        <tr>
            <td class="col-md-2"><strong>ID</strong></td>
            <td><?= $item->id ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Created By</strong></td>
            <td><?= $item->created_by ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Supervisor</strong></td>
            <td><?= $item->supervisor ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Start</strong></td>
            <td><?= $item->start ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>End</strong></td>
            <td><?= $item->end ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Authorized By</strong></td>
            <td><?= $item->authorized_by ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Reason</strong></td>
            <td><?= $item->reason ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Approved</strong></td>
            <td><?= $item->approved ? 'yes' : 'no' ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Approved By</strong></td>
            <td><?= $item->approved_by ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Approved On</strong></td>
            <td><?= $item->approved_on ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Created On</strong></td>
            <td><?= $item->created_on ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Modified On</strong></td>
            <td><?= $item->modified_on ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Deleted</strong></td>
            <td><?= $item->deleted ?></td>
        </tr>
        <tr>
            <td class="col-md-2"><strong>Deleted By</strong></td>
            <td><?= $item->deleted_by ?></td>
        </tr>
    </tbody>
</table>
