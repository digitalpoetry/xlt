<h2>New Time Exception</h2>

<?= form_open(); ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="form-group">
            <label for="">Created By</label>
            <input type="number" class="form-control" name="created_by" />
        </div>
        <div class="form-group">
            <label for="">Supervisor</label>
            <input type="number" class="form-control" name="supervisor" />
        </div>
        <div class="form-group">
            <label for="">Start</label>
            <input type="datetime" class="form-control" name="start" />
        </div>
        <div class="form-group">
            <label for="">End</label>
            <input type="datetime" class="form-control" name="end" />
        </div>
        <div class="form-group">
            <label for="">Authorized By</label>
            <input type="number" class="form-control" name="authorized_by" />
        </div>
        <div class="form-group">
            <label for="">Reason</label>
            <input type="text" class="form-control" name="reason" />
        </div>
        <div class="form-group">
            <label for="">Approved</label>
            <input type="number" class="form-control" name="approved" />
        </div>
        <div class="form-group">
            <label for="">Approved By</label>
            <input type="number" class="form-control" name="approved_by" />
        </div>
        <div class="form-group">
            <label for="">Approved On</label>
            <input type="datetime" class="form-control" name="approved_on" />
        </div>
    </div>
</div>

<input type="submit" class="btn btn-primary" name="submit" value="Submit Time Exception" />
&nbsp;or&nbsp;
<a href="<?= site_url('texceptions') ?>">Cancel</a>

<?= form_close(); ?>
