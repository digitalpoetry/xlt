<h2>Update Time Exception</h2>

<?= form_open(); ?>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-md-4">
        <div class="form-group">
            <label for="">ID</label>
            <input type="number" name="id" class="form-control" value="<?= set_value('id', $item->id ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Created By</label>
            <input type="number" name="created_by" class="form-control" value="<?= set_value('created_by', $item->created_by ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Supervisor</label>
            <input type="number" name="supervisor" class="form-control" value="<?= set_value('supervisor', $item->supervisor ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Start</label>
            <input type="datetime" name="start" class="form-control" value="<?= set_value('start', $item->start ) ?>" />
        </div>
        <div class="form-group">
            <label for="">End</label>
            <input type="datetime" name="end" class="form-control" value="<?= set_value('end', $item->end ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Authorized By</label>
            <input type="number" name="authorized_by" class="form-control" value="<?= set_value('authorized_by', $item->authorized_by ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Reason</label>
            <input type="text" name="reason" class="form-control" value="<?= set_value('reason', $item->reason ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Approved</label>
            <input type="number" name="approved" class="form-control" value="<?= set_value('approved', $item->approved ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Approved By</label>
            <input type="number" name="approved_by" class="form-control" value="<?= set_value('approved_by', $item->approved_by ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Approved On</label>
            <input type="datetime" name="approved_on" class="form-control" value="<?= set_value('approved_on', $item->approved_on ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Deleted</label>
            <input type="number" name="deleted" class="form-control" value="<?= set_value('deleted', $item->deleted ) ?>" />
        </div>
        <div class="form-group">
            <label for="">Deleted By</label>
            <input type="number" name="deleted_by" class="form-control" value="<?= set_value('deleted_by', $item->deleted_by ) ?>" />
        </div>
    </div>
</div>

<input type="submit" name="submit" class="btn btn-primary" value="Update Time Exception" />
&nbsp;or&nbsp;
<a href="<?= site_url('texceptions') ?>">Cancel</a>

<?= form_close(); ?>
