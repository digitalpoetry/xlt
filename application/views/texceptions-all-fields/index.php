<h2>Time Exceptions</h2>

<a href='texceptions/create' class='btn btn-primary ' role='button'>New Time Exception</a>

<hr/>

<?php if ( ! empty($rows) && is_array($rows) && count($rows) ) : ?>

    <?php $headers = [
        'id'            => 'ID',
        'created_by'    => 'Created By',
        'supervisor'    => 'Supervisor',
        'start'         => 'Start',
        'end'           => 'End',
        'authorized_by' => 'Authorized By',
        'reason'        => 'Reason',
        'approved'      => 'Approved',
        'approved_by'   => 'Approved By',
        'approved_on'   => 'Approved On',
        'created_on'    => 'Created On',
        'modified_on'   => 'Modified On',
        'deleted'       => 'Deleted',
        'deleted_by'    => 'Deleted By',
    ]; ?>

    <table class="table table-striped table-hover">
        <thead>

            <tr>
                <?php foreach ($headers as $key => $label) : ?>
                <th><?= $label ?></th>
                <?php endforeach; ?>

                <th>Actions</th>
            </tr>

        </thead>
        <tbody>

            <?php foreach ($rows as $row) : ?>
            <tr>
                <?php foreach ($headers as $key => $label) : ?>
                <td><?= $row[$key] ?></td>
                <?php endforeach; ?>

                <td>
                    <a href="texceptions/update/<?= $row['id'] ?>">Edit</a> |
                    <a href="texceptions/delete/<?= $row['id'] ?>" onclick="return confirm('Delete this item?');">Delete</a>
                </td>
            </tr>
            <?php endforeach; ?>

        </tbody>
    </table>

<?php else : ?>

    <div class='alert alert-warning '>
        Unable to find any records.
        <a href='#' class='close'>&times;</a>
    </div>

<?php endif; ?>
