<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

?><div class="page-header">
    <h2 >New Time Exception <small>Do it meow!</small></h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

<!-- Supervisor -->
<div class="form-group">
    <label class="control-label col-xs-2">Supervisor</label>
    <div class="col-xs-4">
        <select class="form-control" id="supervisor" name="supervisor">
            <option>Supervisor</option>
            <?php foreach ($supervisors as $sup ) : ?>
                <?php $select = $sup['id'] == $item->supervisor ? ' selected' : '' ?>
                <option value="<?= $sup['id'] ?>"<?= $select ?>><?= $sup['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<!-- Start -->
<div class="form-group">
    <label class="control-label col-xs-2">Start</label>
    <div class="col-xs-2">
        <input type="date" class="form-control" id="start_date" value="<?= $now['date'] ?>" />
    </div>
    <div class="col-xs-2">
        <input type="time" class="form-control" id="start_time" value="" />
    </div>
</div>

<!-- Duration -->
<div class="form-group">
    <label class="control-label col-xs-2" for="duration">Duration</label>
    <div class="col-xs-2">
        <div class="input-group">
            <input type="number" class="form-control" id="duration" name="hours" min="0" max="12">
            <span class="input-group-addon">hours</span>
        </div>
    </div>
    <div class="col-xs-2">
        <div class="input-group">
            <input type="number" class="form-control" id="duration" name="minutes" min="0" max="59">
            <span class="input-group-addon">minutes</span>
        </div>
    </div>
</div>

<!-- End -->
<div class="form-group">
    <label class="control-label col-xs-2">End</label>
    <div class="col-xs-2">
        <input type="date" class="form-control" id="end_date" value="<?= $now['date'] ?>" />
    </div>
    <div class="col-xs-2">
        <input type="time" class="form-control" id="end_time" value="<?= $now['time'] ?>" />
    </div>
</div>

<!-- Authorized By -->
<div class="form-group">
    <label class="control-label col-xs-2">Authorized By</label>
    <div class="col-xs-4">
        <select class="form-control" id="authorized_by" name="authorized_by">
            <option>Supervisor</option>
            <?php foreach ($supervisors as $sup ) : ?>
                <?php $select = $sup['id'] == $item->authorized_by ? ' selected' : '' ?>
                <option value="<?= $sup['id'] ?>"<?= $select ?>><?= $sup['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
</div>

<!-- Snippets -->
<div class="form-group form-group-sm">
    <label class="control-label col-xs-2" for="reason">Reason</label>
    <div class="col-xs-4">
        <div class="input-group">
            <select class="form-control" id="snippet" name="snippet">
                <option>select a snippet...</option>
                <?php foreach ($snippets as $snippet ) : ?>
                    <option value="<?= $snippet['value'] ?>"><?= $snippet['name'] ?></option>
                <?php endforeach; ?>
            </select>
            <div class="input-group-btn">
                <button type="button" class="btn btn-default btn-sm" id="insert">Insert</button>
            </div>
        </div>
    </div>
</div>

<!-- Reason -->
<div class="form-group">
    <div class="col-xs-offset-2 col-xs-6">
        <textarea rows="3" class="form-control" id="reason" name="reason"><?= $snippets[$default_snippet]['value'] ?></textarea>
    </div>
</div>

<!-- Submit -->
<div class="form-group">
    <div class="col-xs-offset-2 col-xs-6">
        <input type="submit" name="submit" class="btn btn-primary" value="Add Exception" />
        &nbsp;or&nbsp;
        <a href="<?= site_url('xceptions') ?>">Cancel</a>
    </div>
</div>

<?= form_close(); ?>
