<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><div class="page-header">
    <h2 >Time Exception <small>Is it everything you wished for?</small></h2>
</div>

<div class="content-toolbar pull-right">
    <a href="<?= site_url('admin/texceptions/update/' . $item->id) ?>" class="btn btn-primary" role="button">Edit Exception</a>
    <a href="<?= site_url('admin/texceptions/delete/' . $item->id) ?>" class="btn btn-danger" role="button" onclick="return confirm('Delete this item?');">Delete Exception</a>
</div>

<table class="table table-striped table-condensed">
    <tbody>
    
        <?php foreach ($fields as $field => $label) : ?>
            
        <tr>
            <th class="col-sm-2"><?= $label ?></td>
            <td><?= $item->$field ?></td>
        </tr>
        
        <?php endforeach; ?>
        
    </tbody>
</table>
