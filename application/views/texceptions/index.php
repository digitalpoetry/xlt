<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><div class="page-header">
    <h2 >Time Exceptions <small>It's better than bad, it's good!</small></h2>
</div>

<div class="content-toolbar pull-right">
    <a href="<?= site_url('admin/texceptions/create') ?>" class="btn btn-primary" role="button">New Exception</a>
</div>

<hr />

<?php if ( ! empty($items) && is_array($items) && count($items) ) : ?>

    <div class="table-responsive">
        <table class="table table-hover table-striped table-condensed table-admin">
            <thead>
                <tr>

                    <?php foreach ($fields as $field => $label) : ?>
                    <th><?= $label ?></th>
                    <?php endforeach; ?>
                    <th class="col-sm-2">Actions</th>

                </tr>
            </thead>
            <tbody>
            
                <?php foreach ($items as $item) : ?>
                <tr>
                    <?php foreach ($fields as $field => $label) : ?>
                    <td><?= $item->$field ?></td>
                    <?php endforeach; ?>
                    <td class="table-actions">
                        <a class="text-primary" href="<?= site_url('admin/texceptions/show/' . $item->id)   ?>">View</a> |
                        <a class="text-info" href="<?= site_url('admin/texceptions/update/' . $item->id) ?>">Edit</a> |
                        <a class="text-danger" href="<?= site_url('admin/texceptions/delete/' . $item->id) ?>" onclick="return confirm('Delete this item?');">Delete</a>
                    </td>
                </tr>
                <?php endforeach; ?>

            </tbody>
        </table>
    </div>

<?php else : ?>

    <div class="alert alert-warning">
        Unable to find any records. <a href="#" class="close">&times;</a>
    </div>

<?php endif; ?>
