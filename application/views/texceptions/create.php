<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\View
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource  
 */


?><div class="page-header">
    <h2 >New Time Exception <small>Do it meow!</small></h2>
</div>

<?= form_open('', array('class' => 'form-horizontal')); ?>

    <div class="row">

        <!-- Supervisor -->
        <div class="form-group">
            <label for="supervisor" class="col-md-2 control-label">Supervisor</label>
            <div class="col-md-4">
                <select class="form-control" id="supervisor" name="supervisor">
                    <option>select</option>
                    <?php foreach ($supervisor_list as $option) : ?>
                    <?php $selected = is_option_selected('supervisor', $option->id, $item->supervisor) ?>
                    <option value="<?= $option->id ?>"<?= $selected ?>><?= get_user_fullname($option->id) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <!-- Start -->
        <div class="form-group">
            <label class="col-md-2 control-label" for="start">Start</label>
            <div class="col-md-4">
                <input type="datetime-local" class="form-control" id="start" name="start" step="60" value="<?= set_value('start', $start_default) ?>" />
            </div>
        </div>

        <!-- End -->
        <div class="form-group">
            <label for="end" class="col-md-2 control-label">End</label>
            <div class="col-md-4">
                <input type="datetime-local" class="form-control" id="end" name="end" step="60" value="<?= set_value('end', $end_default) ?>" />
            </div>
        </div>

        <!-- Authorized By -->
        <div class="form-group">
            <label for="authorized_by" class="col-md-2 control-label">Authorized By</label>
            <div class="col-md-4">
                <select class="form-control" id="authorized_by" name="authorized_by">
                    <option value="">select</option>
                    <?php foreach ($supervisor_list as $option) : ?>
                    <?php $selected = is_option_selected('authorized_by', $option->id) ?>
                    <option value="<?= $option->id ?>"<?= $selected ?>><?= get_user_fullname($option->id) ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <!-- Reason -->
        <div class="form-group">
            <label for="reason" class="col-md-2 control-label">Reason</label>
            <div class="col-md-10">
                <input type="text" class="form-control" id="reason" name="reason" value="<?= set_value('reason', '' ) ?>" placeholder="<?= lang('form_reason_placeholder') ?>" />
            </div>
        </div>

        <!-- Submit -->
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="submit" class="btn btn-primary" name="submit" value="Submit Exception" />
                &nbsp;or&nbsp;
                <a href="<?= site_url('admin/texceptions') ?>">Cancel</a>
            </div>
        </div>

    </div>

<?= form_close(); ?>
