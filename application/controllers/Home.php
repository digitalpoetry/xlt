<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Controller
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 */

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @inheritdoc
 */
class Home extends \Myth\Controllers\ThemedController {

    /**
     * The type of caching to use. The default values are
     * set globally in the environment's start file, but
     * these will override if they are set.
     * 
     * @var null $cache_type
     * @var null $backup_cache
     */
    protected $cache_type   = null;
    protected $backup_cache = null;

    /**
     * @var null $language_file If set, this language file will automatically be loaded.
     */
    protected $language_file = null;

    /**
     * @var null $model_file If set, this model file will automatically be loaded.
     */
    protected $model_file = 'texception_model';

    /**
     * @var null $theme Allows per-controller override of theme.
     */
    protected $theme = null;

    /**
     * @var null $layout Per-controller override of the current layout file.
     */
    protected $layout = 'two_left';

    /**
     * @var string $uikit The UIKit to make available to the template views.
     */
    protected $uikit = '';

    /**
     * @var int $limit The number of rows to show when paginating results.
     */
    protected $limit = 25;


    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *     http://example.com/index.php/welcome
     * - or -
     *     http://example.com/index.php/welcome/index
     * - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public function index()
    {
        $this->render();
    }
}
