<?php
/**
 * Code All The Things!
 *
 * A project kickstarter based on the Sprint & CodeIgnitor frameworks.
 *
 * @package     DigitalPoetry\CATT\Model
 * @author      Jesse LaReaux <jlareaux@gmail.com>
 * @copyright   Copyright (c) 2016, DigitalPoetry (http://digitalpoetry.studio/).
 * @license     http://opensource.org/licenses/MIT MIT License
 * @link        http://codeallthethings.xyz
 * @version     0.1.0 Shiny Things
 * @filesource
 *
 * @todo  I10n strings in this file
 */


use Myth\Models\CIDbModel;

/**
 * Texception Model.
 */
class Texception_model extends AdminModel {

    /**
     * Database table name for this model.
     * 
     * @var String
     * @todo Set $table_name to the classname, if possible set in the parent class.
     */
    protected $table_name  = 'texceptions';

    /**
     * @var Boolean $soft_deletes Set true to enable soft deletes.
     */
    protected $soft_deletes = true;

    /**
     * Various callbacks available to the class. They are simple lists of
     * method names (methods will be ran on $this).
     */
    /*protected $before_insert = array(
        'set_created_by',
        'set_authorized_by',
        'set_approved_fields'
    );
    protected $before_update = array(
        'set_authorized_by',
        'set_approved_fields'
    );*/

    /**
     * If set to true, logs the user id for 'created_by', 'modified_by' and 'deleted_by'.
     * 
     * @var Boolean $log_user Set true to enable user logging.
     */
    protected $log_user = true;

    /**
     * List of fields in the table. This can be set to avoid a database call if 
     * using $this->prep_data().
     * 
     * @var Array
     */
    protected $fields = [
        'id', 'created_by', 'authorized_by', 'supervisor', 'start', 
        'end', 'reason', 'approved', 'approved_by', 'approved_on', 
        'created_on', 'modified_on', 'deleted', 'deleted_by'
    ];

    /**
     * Protected, non-modifiable fields.
     * 
     * @var Array
     */
    protected $protected_attributes = [
        'id',
        /* 'created_by', */
        'submit'
    ];

    /**
     * An array of validation rules, same format used with Form_validation library.
     * 
     * @var Array 
     */
    protected $validation_rules = [
        ['field' => 'authorized_by', 'label' => 'Authorized By', 'format' => 'username',  'rules' => 'trim|integer|max_length[9]'],
        ['field' => 'supervisor',    'label' => 'Supervisor',    'format' => 'username',  'rules' => 'trim|required|integer|max_length[9]'],
        ['field' => 'start',         'label' => 'Start',         'format' => 'datetime',  'rules' => 'trim|required|exact_length[16]'],
        ['field' => 'end',           'label' => 'End',           'format' => 'datetime',  'rules' => 'trim|required|exact_length[16]'],
        ['field' => 'reason',        'label' => 'Reason',        'format' => 'htmlchars', 'rules' => 'trim|required|min_length[3]|max_length[255]'],
        ['field' => 'approved',      'label' => 'Approved',      'format' => 'boolean',   'rules' => 'trim|integer|in_list[0,1]'],
        ['field' => 'approved_by',   'label' => 'Approved By',   'format' => 'username',  'rules' => 'trim|integer|max_length[9]'],
        ['field' => 'approved_on',   'label' => 'Approved On',   'format' => 'datetime',  'rules' => 'trim|exact_length[16]'],
        ['field' => 'created_on',    'label' => 'Submitted On',  'format' => 'datetime',  'rules' => 'trim|exact_length[16]'],
        ['field' => 'modified_on',   'label' => 'Modified On',   'format' => 'datetime',  'rules' => 'trim|exact_length[16]'],
        ['field' => 'deleted',       'label' => 'Deleted',       'format' => 'boolean',   'rules' => 'trim|in_list[0,1]'],
        ['field' => 'deleted_by',    'label' => 'Deleted By',    'format' => 'username',  'rules' => 'trim|integer|max_length[9]']
    ];

    /**
     * Additional validation rules used during inserts only.
     * 
     * @var Array
     */
    protected $insert_validate_rules = [];


    /**
     * @inheritdoc
     */
    public function __construct()
    {
        parent::__construct();

        $this->benchmark->mark('Initialized Texceptions Model');
    }


    /**
     * Callback method for 'created_by'.
     * 
     * A callback method which sets the 'created_by' field with the current
     * user's ID.
     * 
     * @param array $row The array of data to be inserted
     *
     * @return array
     */
    protected function set_created_by($row)
    {
        /*
        if (empty($row['fields']))
        {
            return null;
        }

        $row = $row['fields'];

        // Created_on
        if (! array_key_exists($this->created_field, $row))
        {
            $row[$this->created_field] = $this->set_date();
        }

        // Created by
        if ($this->log_user && ! array_key_exists($this->created_by_field, $row) && is_object($this->authenticate))
        {
            // If you're here because of an error with $this->authenticate
            // not being available, it's likely due to you not using
            // the AuthTrait and/or setting log_user after model is instantiated.
            $row[$this->created_by_field] = (int)$this->authenticate->id();
        }

        return $row;
        */
    }

    /**
     * Callback method for 'authorized_by'.
     * 
     * A callback method which sets 'authorized_by' to null if it's value is an 
     * empty string.
     */
    protected function set_authorized_by($row)
    {
        d($row);
    }

    /**
     * Callback method for 'approved_on' and 'approved_by'.
     * 
     * A callback method which checks the 'approved' field and will set the
     * 'approved_on' and 'approved_by' accordingly.
     */
    protected function set_approved_fields()
    {
        #
    }

}