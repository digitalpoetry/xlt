# Code All The Things!

Based on the CodeIgniter 3 PHP framework & Spring PHP.

## System Requirements

- PHP version 5.4 or newer.
- SimpleXML enabled on your PHP installation for the docs to work.
- Composer installed on development server.
- A Database. We currently use MySQL but try to keep it fairly flexible.

## How To Install?

## Where's the Docs?
